<?php
/**
 * The template for displaying all single posts.
 *
 * @package Portfolio 3
 */

get_header(); ?>

<div id="primary" class="contenido">
		<main id="main" class="contenido-principal" role="main">

		<?php while ( have_posts() ) : the_post(); ?>


			<?php if(is_singular('blog')){ ;?>
				<?php
				if ( has_post_thumbnail() ) { ;?>
					<div class="entrada__cover" style="background-image:url('<?php miniatura('large'); ?>');"></div>
				<?php
				}
				else {
				}
				?>
			<?php } ;?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="contenedor">

				<header class="entrada__header">
					<h1 class="entrada__titulo"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

					<div class="entrada__meta">
						<?php tags();?>

						<?php if(is_singular('blog')){
							echo '<p>'. the_date().'</p>';
							 tax('tema');
							}
							;?>

					</div><!-- .entry-meta -->
				</header><!-- .entry-header -->

				<div class="entrada__contenido">
					<?php the_content(); ?>
					<?php
						wp_link_pages( array(
							               'before' => '<div class="page-links">' . __( 'Pages:', 'portfolio-3' ),
							               'after'  => '</div>',
						               ) );
					?>
				</div><!-- .entry-content -->

				<footer class="entrada__footer">
					<?php portfolio_3_entry_footer(); ?>
				</footer><!-- .entry-footer -->
</div>
			</article><!-- #post-## -->
			<?php nav_posteos(); ?>


		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
