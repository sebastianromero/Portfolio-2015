'use strict';
var gulp = require('gulp');
var coffee = require('gulp-coffee');//dependencia de gulp-notify
var filter = require('gulp-filter');
var notify = require("gulp-notify");
var pixrem = require('gulp-pixrem');
var plumber = require('gulp-plumber');
var prefix = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');


//Sass
gulp.task('scss', function () {
    gulp.src(['scss/**/*.scss', 'scss/*.scss'])
        .pipe(plumber({errorHandler: notify.onError("<%= error.message %>")}))
        .pipe(sass({
            lineNumbers: true,
            errLogToConsole: true
        }))
        .pipe(prefix("last 4 versions", "> 5%", "ie 8", "ie 7", "ios 3"))
        .pipe(pixrem('22px'))
        .pipe(minify({
            keepSpecialComments:true
        }
            ))
        .pipe(filter('**/*.css'))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.reload({stream: true}));
});



//Browsersync
gulp.task('browser-sync', function () {
    browserSync({
        proxy: "localhost/p",
        logLevel: "debug"
    });
});



//Javascript
gulp.task('js', function () {
    gulp.src(['js/*.js', '!js/scripts.js'])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'))
        .pipe(browserSync.reload({stream: true}))

});




gulp.task('watch', function () {
    gulp.watch(['scss/**/*.scss', 'scss/*.scss'], ['scss'], reload);
    gulp.watch(['*.php', './inc/*.php'], reload);
gulp.watch(['js/*.js', '!js/scripts.js'], ['js'], reload);

});


gulp.task('default', ['browser-sync', 'scss', 'js', 'watch' ], function () {
});
