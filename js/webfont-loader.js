(function($) {
			WebFontConfig = {
				google: { families: ['Roboto:500,500italic,300,300italic:latin', 'Merriweather:400,400italic,700italic,700:latin', ] }
                //,
				//custom: {
				//	families: ['Free+Sans'],
				//	urls: [''+urlTema+'/fonts/clear-sans/stylesheet.css']
				//}

			};
			(function() {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})();
		}(jQuery));