(function($) {
$(function() {
    $('.slider').flexslider(
        {
            animation: "fade",
            animationLoop: false,
            slideshowSpeed: 8000,
            animationSpeed: 800,
            touch: true,
            keyboard: true

        }
    );
});
}(jQuery));