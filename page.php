<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Portfolio 3
 */

get_header(); ?>

	<div id="primary" class="contenido">
		<main id="main" class="contenido-principal" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class('pagina contenedor'); ?>>

				<div class="entrada__contenido">
					<?php the_content(); ?>


					<?php if(is_page('contacto')){;
//Mostrar los datos de contacto en la pagina correspondiente
						?>
						<div class="contactos">

							<p><a href="mailto:sebastianromerog@gmail.com">sebastianromerog@gmail.com</a>
							</p>
							<p>
								<?php $telefono=get_user_meta( 1, 'telefono', true ) ;
									echo $telefono?>
								<br><br>
							</p>


							<p>
								<?php $behance=get_user_meta( 1, 'behance', true ) ;
									echo '<a href="http://www.behance.com/'.$behance.'">Behance</a>' ?>
							</p>
							<p>
								<?php $twitter=get_user_meta( 1, 'twitter', true ) ;
									echo '<a href="http://www.twitter.com/'.$twitter.'">Twitter</a>' ?>
							</p>
							<p>
								<?php $facebook=get_user_meta( 1, 'facebook', true ) ;
									echo '<a href="https://www.facebook.com/'.$facebook.'">Facebook</a>' ?>
							</p>
							<p>
								<?php $flickr=get_user_meta( 1, 'flickr', true ) ;
									echo '<a href="http://www.flickr.com/'.$flickr.'/sets">Flickr</a>' ?>
							</p>
							<p>
								<?php $vimeo=get_user_meta( 1, 'vimeo', true ) ;
									echo '<a href="http://www.vimeo.com/'.$vimeo.'">Vimeo</a>' ?>
							</p>


						</div>

					<?php } ;?>




					<?php
						wp_link_pages( array(
							               'before' => '<div class="page-links">' . __( 'Pages:', 'portfolio-3' ),
							               'after'  => '</div>',
						               ) );
					?>
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php edit_post_link( __( 'Editar', 'portfolio-3' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>