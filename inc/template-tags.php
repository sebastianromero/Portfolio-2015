<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Portfolio 3
 */


function nav_posteos() {; ?>
	<nav class="nav-posteos" role="navigation">
		<div class="nav-posteos__wrap contenedor">
			<?php
				if(is_singular('blog')){
echo '<div class="nav-posteos__link nav-posteos__link--izquierda">';
				previous_post_link( '
					<span>Post anterior<br></span>
					%link', '%title' );
					echo '</div>';

				echo '<div class="nav-posteos__link nav-posteos__link--derecha">';
				next_post_link( '
				<span>Post siguiente<br></span>
					%link', '%title' );
					echo '</div>';
					}

					else{
					echo '<div class="nav-posteos__link nav-posteos__link--izquierda">';
					previous_post_link( '
					<span>Proyecto anterior<br></span>
					%link', '%title' );
					echo '</div>';

					echo '<div class="nav-posteos__link nav-posteos__link--derecha">';
				next_post_link( '
				<span>Proyecto siguiente<br></span>
					%link', '%title' );
					echo '</div>';
					};



			?>
		</div><!-- .nav-posteos__links -->
	</nav><!-- .navigation -->
	<?php
}


if ( ! function_exists( 'entrada__meta' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function entrada__meta() {
	
// /* translators: used between list items, there is a space after the comma */
// 		$categories_list = get_the_category_list( __( ', ', 'portfolio-3' ) );
// 		if ( $categories_list && portfolio_3_categorized_blog() ) {
// 			printf( '<p>' . __( '%1$s', 'portfolio-3' ) . '</p>', $categories_list );
// 		}

		/* translators: used between list items, there is a space after the comma */

		$tags_list = get_the_tag_list( '', __( '<br>', 'portfolio-3' ) );
		if ( $tags_list ) {
			printf( '<p class="tags-links">' . __( '%1$s', 'portfolio-3' ) . '</p>', $tags_list );
		}





}
endif;

if ( ! function_exists( 'portfolio_3_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function portfolio_3_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {}
	edit_post_link( __( 'Editar', 'portfolio-3' ), '<span class="edit-link">', '</span>' );
}
endif;

if ( ! function_exists( 'the_archive_title' ) ) :
/**
 * Shim for `the_archive_title()`.
 *
 * Display the archive title based on the queried object.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the title. Default empty.
 * @param string $after  Optional. Content to append to the title. Default empty.
 */
function the_archive_title( $before = '', $after = '' ) {
	if ( is_category() ) {
		$title = sprintf( __( 'Categoría %s', 'portfolio-3' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( 'Tag: %s', 'portfolio-3' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( 'Author: %s', 'portfolio-3' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( 'Year: %s', 'portfolio-3' ), get_the_date( _x( 'Y', 'yearly archives date format', 'portfolio-3' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( 'Month: %s', 'portfolio-3' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'portfolio-3' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( 'Day: %s', 'portfolio-3' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'portfolio-3' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'portfolio-3' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'portfolio-3' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( 'Archivo | %s', 'portfolio-3' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s', 'portfolio-3' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archivo | ', 'portfolio-3' );
	}

	/**
	 * Filter the archive title.
	 *
	 * @param string $title Archive title to be displayed.
	 */
	$title = apply_filters( 'get_the_archive_title', $title );

	if ( ! empty( $title ) ) {
		echo $before . $title . $after;
	}
}
endif;

if ( ! function_exists( 'the_archive_description' ) ) :
/**
 * Shim for `the_archive_description()`.
 *
 * Display category, tag, or term description.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the description. Default empty.
 * @param string $after  Optional. Content to append to the description. Default empty.
 */
function the_archive_description( $before = '', $after = '' ) {
	$description = apply_filters( 'get_the_archive_description', term_description() );

	if ( ! empty( $description ) ) {
		/**
		 * Filter the archive description.
		 *
		 * @see term_description()
		 *
		 * @param string $description Archive description to be displayed.
		 */
		echo $before . $description . $after;
	}
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function portfolio_3_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'portfolio_3_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'portfolio_3_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so portfolio_3_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so portfolio_3_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in portfolio_3_categorized_blog.
 */
function portfolio_3_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'portfolio_3_categories' );
}
add_action( 'edit_category', 'portfolio_3_category_transient_flusher' );
add_action( 'save_post',     'portfolio_3_category_transient_flusher' );
