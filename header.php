<?php
/**
 *
 * @package Portfolio 3
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script type="text/javascript">
		var urlTema = '<?= get_bloginfo("template_url"); ?>';
	</script>
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div class="sitio">
<header class="sitio__header" role="banner">
	<div class="wrap">

			<div class="sitio__logo">
				<h1 class="sitio__nombre"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h1 class="sitio__nombre--short"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">S</a></h1>
			</div>

			<nav  class="sitio__nav" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav>
	</div>
</header>

	<div class="sitio__contenido">