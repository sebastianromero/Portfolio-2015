<?php get_header(); ?>
	<div class="slider__wrap">
		<div class="slider">
			<ul class="slides">
				<?php
					// WP_Query arguments
					$sticky = get_option( 'sticky_posts' );
					$args   = array(
						'posts_per_page' => '-1',
						'post__in'       => $sticky
					);

					// The Query
					$portfolio = new WP_Query( $args );

					// The Loop
					if ( $portfolio->have_posts() ) {
						while ( $portfolio->have_posts() ) {
							$portfolio->the_post();
							?>
							<li class="slider__item" style="background-image:url('<?php miniatura( "full" ); ?>');";">
							<a href="<?php the_permalink(); ?>" class="pseudo-link"></a>

							<div class="slider__project-info">
								<h1><?php the_title(); ?></h1>
							</div>
						</li>
					<?php
					}
					} else {
					}

					// Restore original Post Data
					wp_reset_postdata();;?>
			</ul>
		</div>
	</div>



	<main id="main" class="contenido-principal" role="main">
		<section class="portfolio contenedor">
			<ul class="grilla-portfolio">
				<?php
					// WP_Query arguments
					$sticky = get_option( 'sticky_posts' );
					$args   = array(
						'posts_per_page'      => '6',
						'ignore_sticky_posts' => 1,
						'post_status'         => 'publish',
						'has_password'        => false,
						//				'post__not_in'        => $sticky,


					);

					// The Query
					$portfolio = new WP_Query( $args );

					// The Loop
					if ( $portfolio->have_posts() ) {
						while ( $portfolio->have_posts() ) {
							$portfolio->the_post();
							?>
							<li class="grilla-portfolio__proyecto">
								<article class="proyecto">
									<a href="<?php the_permalink(); ?>" class="pseudo-link"></a>

									<div class="proyecto__imagen" style="background-image:url('<?php miniatura( 'large' ); ?>');"></div>
									<div class="proyecto__datos">
										<h1>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</h1>

									</div>
								</article>
							</li>
						<?php
						}
					} else {
						// no posts found
					}

					// Restore original Post Data
					wp_reset_postdata();;?>
			</ul>
			<?php
				$id   = get_page_by_path( 'portfolio' );
				$page = get_page_link( $id );
				echo '<p class="todos-los-proyectos"><a href="' . $page . '">Ver todos los proyectos</a></p>';
			?>


		</section>
	</main><!-- #main -->
	<aside class="modulos-sociales">
<div class="contenedor">

		<?php
			// WP_Query arguments
			$args = array(
				'post_type'           => 'blog',
				'posts_per_page'      => '1',
				'ignore_sticky_posts' => 1,
			);

			// The Query
			$ultimo_post = new WP_Query( $args );

			// The Loop
			if ( $ultimo_post->have_posts() ) {
				while ( $ultimo_post->have_posts() ) {
					$ultimo_post->the_post();
					?>

					<div class="modulo-social modulo-social--blog">
						<div class="modulo-social__imagen" style="background-image:url('<?php miniatura( full ); ?>');"></div>
						<div class="modulo-social__contenido modulo-social__contenido--con-fondo">
							<h2 class="modulo-social__id">Blog</h2>
							<a href="<?php the_permalink(); ?>" class="pseudo-link"></a>
							<h1>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h1>
						</div>
					</div>

				<?php
				}
			} else {

			}

			// Restore original Post Data
			wp_reset_postdata();;?>


		<div class="modulo-social" id="modulo-social--instagram">

			<?php $instagram = get_user_meta( 1, 'instagram', true );
				echo '<a class="pseudo-link" href="http://www.instagram.com/' . $instagram . '"></a>' ?>

			<div id="instagram__imagen"></div>

			<div class="modulo-social__contenido modulo-social__contenido--con-fondo">
				<h2 class="modulo-social__id">Instagram</h2>

				<h1 id="instagram__caption"></h1>

			</div>
		</div>

	<div class="modulo-social modulo-social--twitter" id="modulo-social--twitter">
		<?php $twitter = get_user_meta( 1, 'twitter', true );
			echo '<a class="pseudo-link" href="http://www.twitter.com/' . $twitter . '"></a>' ?>
		<div class="modulo-social__contenido">
			<h2 class="modulo-social__id">Twitter</h2>

			<div id="twitter__contenido"></div>
		</div>
	</div>



</div>



	</aside>

<?php get_footer(); ?>