<?php get_header(); ?>
<div id="primary" class="contenido contenedor">
	<main id="main" class="contenido-principal" role="main">
<div class="categorias">
<?php nav_categorias(); ?>
</div>

		<section class="portfolio">
			<ul class="grilla-portfolio">
				<?php
			// WP_Query arguments
				$args = array (
					'posts_per_page'         => '-1',
					'ignore_sticky_posts'	 => 'true',
					'post_status' => 'publish',
					'has_password' => false
					);

			// The Query
				$proyectos = new WP_Query( $args );

			// The Loop
				if ( $proyectos->have_posts() ) {
					while ( $proyectos->have_posts() ) {
						$proyectos->the_post();
						?>
<li class="grilla-portfolio__proyecto">
	<article class="proyecto">
		<a href="<?php the_permalink(); ?>" class="pseudo-link"></a>
		<div class="proyecto__imagen" style="background-image:url('<?php miniatura('large'); ?>');"></div>
		<div class="proyecto__datos">
			<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		</div>
	</article>
</li>
						<?php
					}
				} else {
				// no posts found
				}

			// Restore original Post Data
				wp_reset_postdata();
				;?>
			</ul>
		</section>

	</main>
	<!-- #main -->
</div>
<?php get_footer(); ?>