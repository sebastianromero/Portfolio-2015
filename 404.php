<?php get_header(); ?>
<div id="primary" class="contenedor">
	<main id="main" class="site-main" role="main">
		<section>
			<header>
				<h1 class="page-title"><?php _e( 'Me parece que acá no hay nada.', 'portfolio-3' ); ?></h1>
				<p>Fijate que el link esté bien escrito.</p>
			</header>
			<!-- .page-content -->
		</section>
		<!-- .error-404 -->
	</main>
	<!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
