<?php get_header(); ?>

<div id="primary" class="contenido contenedor">
<main id="main" class="contenido-principal" role="main">

	<?php nav_categorias();?>

	<section class="portfolio">

			<ul class="grilla-portfolio">

						
				<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
						<?php if ( ! post_password_required() ) : ?>
<li class="grilla-portfolio__proyecto">
	<article class="proyecto">
		<a href="<?php the_permalink(); ?>" class="pseudo-link"></a>
		<div class="proyecto__imagen" style="background-image:url('<?php miniatura('large'); ?>');"></div>
		<div class="proyecto__datos">
			<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		</div>
	</article>
</li>
						<?php endif; ?>
			<?php endwhile; ?>


		<?php else : ?>


		<?php endif; ?>



					</ul>	

	</section>
	</main>
	<!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>