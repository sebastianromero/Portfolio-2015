<?php
/**
 * Portfolio 3 functions and definitions
 *
 * @package Portfolio 3
 */

	add_filter('protected_title_format', 'blank');
	function blank($title) {
		return '%s';
	}


if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'portfolio_3_setup' ) ) :

	function portfolio_3_setup() {
		load_theme_textdomain( 'portfolio-3', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'portfolio-3' ),
			) );
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
			) );

	}
endif; // portfolio_3_setup
add_action( 'after_setup_theme', 'portfolio_3_setup' );


/**
 * Enqueue scripts and styles.
 */
function portfolio_3_scripts() {
	wp_enqueue_style( 'portfolio-3-style', get_stylesheet_uri() );

	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'portfolio_3_scripts' );

require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/customizer.php';



update_option('image_default_link_type','none');

add_theme_support( 'post-thumbnails' );


	function tax($taxonomia){

$terms = get_the_terms( $post->ID , $taxonomia );
		echo '<ul class="tags-links">';
                    foreach ( $terms as $term ) {
	                    $term_link = get_term_link( $term, $taxonomia );
	                    if( is_wp_error( $term_link ) )
		                    continue;
	                    echo '<li>'. $term->name . '</li>';
                    }
		echo '</ul>';

	}


	function tags(){
    $posttags = get_the_tags();
    if ($posttags) {
	    echo '<ul class="tags__lista">';
	    foreach($posttags as $tag) {
		    echo '<li>'.$tag->name . '</li>';
	    }
	    echo '</ul>';
    }

	}

function miniatura($size = "thumbnail"){

	$url=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "$size");
	echo $url['0'];
}

function miniatura_size(){
	$tamano=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	echo $tamano['1'] . 'px ';
	echo $tamano['2'] . 'px';
}

// add_filter( 'pre_option_category_base', 'categoria_base' );
// function categoria_base( $value ) {
// 	return 'disciplina';

// }

add_filter( 'pre_option_tag_base', 'tags_base' );
function tags_base( $value ) {
	return 'servicio';

}


function nav_categorias(){
	//$id= get_page_by_path('portfolio');

	echo '<section class="nav-categorias">
	<ul class="nav-categorias__listado">';
if(is_page('portfolio') || is_category()){
		$args = array(
			'hide_empty'         => 1,
			'use_desc_for_title' => 0,
			'exclude'            => '1',
			'title_li'           => '',
			'taxonomy'           => 'category',
			);
		wp_list_categories( $args );
};
	if(is_post_type_archive() || is_tax()){
		$args = array(
			'hide_empty'         => 0,
			'use_desc_for_title' => 0,
			'exclude'            => '1',
			'title_li'           => '',
			'taxonomy'           => 'tema',
		);
		wp_list_categories( $args );
	}
	//echo '<li><a href="' . get_page_link($id) .' ">Todos</a></li>';
		echo '</ul>
	</section>';
}

////Desactivar los tags de parrafo automaticos
//remove_filter( 'the_content', 'wpautop' );




	function filter_ptags_on_images($content){
		return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	}

	add_filter('the_content', 'filter_ptags_on_images');


// Registrar medios de contacto para los usuarios
	function contacto( $user_contact_method ) {

		$user_contact_method['facebook'] = 'Facebook';
		$user_contact_method['twitter'] = 'Twitter';
		$user_contact_method['pinterest'] = 'Pinterest';
		$user_contact_method['telefono'] = 'Teléfono';
		$user_contact_method['flickr'] = 'Flickr';
		$user_contact_method['vimeo'] = 'Vimeo';
		$user_contact_method['behance'] = 'Behance';
		$user_contact_method['github'] = 'Github';
		$user_contact_method['instagram'] = 'Instagram';
		return $user_contact_method;
	}

// Hook into the 'user_contactmethods' filter
	add_filter( 'user_contactmethods', 'contacto' );




// Register Custom Post Type
function blog() {

	$labels = array(
		'name'                => 'Blog',
		'singular_name'       => 'Post',
		'menu_name'           => 'Blog',
		'parent_item_colon'   => 'Parent Item:',
		'all_items'           => 'All Items',
		'view_item'           => 'View Item',
		'add_new_item'        => 'Add New Item',
		'add_new'             => 'Add New',
		'edit_item'           => 'Edit Item',
		'update_item'         => 'Update Item',
		'search_items'        => 'Search Item',
		'not_found'           => 'Not found',
		'not_found_in_trash'  => 'Not found in Trash',
	);
	$args = array(
		'label'               => 'blog',
		'description'         => 'Blog',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'categorias_blog' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-lightbulb',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'blog', $args );

}

// Hook into the 'init' action
add_action( 'init', 'blog', 0 );


// Register Custom Taxonomy
function categorias_blog() {

	$labels = array(
		'name'                       => 'Temas',
		'singular_name'              => 'Tema',
		'menu_name'                  => 'Temas',
		'all_items'                  => 'All Items',
		'parent_item'                => 'Parent Item',
		'parent_item_colon'          => 'Parent Item:',
		'new_item_name'              => 'New Item Name',
		'add_new_item'               => 'Add New Item',
		'edit_item'                  => 'Edit Item',
		'update_item'                => 'Update Item',
		'separate_items_with_commas' => 'Separate items with commas',
		'search_items'               => 'Search Items',
		'add_or_remove_items'        => 'Add or remove items',
		'choose_from_most_used'      => 'Choose from the most used items',
		'not_found'                  => 'Not Found',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tema', array( 'blog' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'categorias_blog', 0 );
