<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Portfolio 3
 */
?>

</div><!-- #content -->

<footer  class="sitio__footer footer" role="contentinfo">
	
<div class="footer__modulo footer__modulo--extendido">
		<ul>
			<li>
				<a href="mailto:sebastianromerog@gmail.com">sebastianromerog@gmail.com</a>
			</li>
			<li>
				<?php $telefono=get_user_meta( 1, 'telefono', true ) ;
				echo $telefono?>
			</li>

		</ul>
	</div>


	<div class="footer__modulo">
		<ul>
			<li>
				<?php $behance=get_user_meta( 1, 'behance', true ) ;
				echo '<a href="http://www.behance.com/'.$behance.'">Behance</a>' ?>
			</li>
			<li>
				<?php $twitter=get_user_meta( 1, 'twitter', true ) ;
				echo '<a href="http://www.twitter.com/'.$twitter.'">Twitter</a>' ?>
			</li>
			<li>
				<?php $facebook=get_user_meta( 1, 'facebook', true ) ;
				echo '<a href="https://www.facebook.com/'.$facebook.'">Facebook</a>' ?>
			</li>
		</ul>
		</div>

		

</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
